-- MySQL dump 10.13  Distrib 5.6.46, for osx10.14 (x86_64)
--
-- Host: localhost    Database: yxx_admin_2
-- ------------------------------------------------------
-- Server version	5.6.46

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alembic_version`
--

DROP TABLE IF EXISTS `alembic_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alembic_version` (
  `version_num` varchar(32) NOT NULL,
  PRIMARY KEY (`version_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alembic_version`
--

LOCK TABLES `alembic_version` WRITE;
/*!40000 ALTER TABLE `alembic_version` DISABLE KEYS */;
INSERT INTO `alembic_version` VALUES ('8393d13050d6');
/*!40000 ALTER TABLE `alembic_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carriages`
--

DROP TABLE IF EXISTS `carriages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carriages` (
  `carriage_id` varchar(28) NOT NULL,
  `make_company` varchar(128) DEFAULT NULL,
  `marshall_way` varchar(128) DEFAULT NULL,
  `marshall_info` varchar(128) DEFAULT NULL,
  `buy_bacth` varchar(128) DEFAULT NULL,
  `power_supply_way` varchar(128) DEFAULT NULL,
  `seat_capacity` varchar(128) DEFAULT NULL,
  `bogie` varchar(128) DEFAULT NULL,
  `reserve_1` int(11) DEFAULT NULL,
  `reserve_2` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`carriage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carriages`
--

LOCK TABLES `carriages` WRITE;
/*!40000 ALTER TABLE `carriages` DISABLE KEYS */;
/*!40000 ALTER TABLE `carriages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dim_city`
--

DROP TABLE IF EXISTS `dim_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dim_city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_code` varchar(20) NOT NULL,
  `city_name_cn` varchar(128) NOT NULL,
  `city_name_en` varchar(128) NOT NULL,
  `province_id` varchar(20) NOT NULL,
  `country_id` varchar(20) NOT NULL,
  `reserve_1` int(11) DEFAULT NULL,
  `reserve_2` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dim_city`
--

LOCK TABLES `dim_city` WRITE;
/*!40000 ALTER TABLE `dim_city` DISABLE KEYS */;
INSERT INTO `dim_city` VALUES (1,'SHA','上海','shanghai','31','156',NULL,NULL),(2,'CKG','重庆','chongqing','50','156',NULL,NULL),(3,'CAN','广州','guangzhou','44','156',NULL,NULL);
/*!40000 ALTER TABLE `dim_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dim_country`
--

DROP TABLE IF EXISTS `dim_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dim_country` (
  `country_id` varchar(20) NOT NULL,
  `country_2code` varchar(20) NOT NULL,
  `country_3code` varchar(20) NOT NULL,
  `country_name_cn` varchar(128) NOT NULL,
  `country_name_cn_short` varchar(128) NOT NULL,
  `country_name_en` varchar(128) NOT NULL,
  `reserve_1` int(11) DEFAULT NULL,
  `reserve_2` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dim_country`
--

LOCK TABLES `dim_country` WRITE;
/*!40000 ALTER TABLE `dim_country` DISABLE KEYS */;
INSERT INTO `dim_country` VALUES ('156','CH','CHN','中华人民共和国','中国','China',NULL,NULL),('784','AE','ARE','阿拉伯联合酋长国','阿联酋','The United Arab Emirates',NULL,NULL),('840','US','USA','美利坚合众国','美国','United States of America',NULL,NULL);
/*!40000 ALTER TABLE `dim_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dim_province`
--

DROP TABLE IF EXISTS `dim_province`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dim_province` (
  `province_id` varchar(20) NOT NULL,
  `province_code` varchar(20) NOT NULL,
  `province_name_cn` varchar(128) NOT NULL,
  `province_name_en` varchar(128) NOT NULL,
  `country_id` varchar(20) NOT NULL,
  `reserve_1` int(11) DEFAULT NULL,
  `reserve_2` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`province_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dim_province`
--

LOCK TABLES `dim_province` WRITE;
/*!40000 ALTER TABLE `dim_province` DISABLE KEYS */;
INSERT INTO `dim_province` VALUES ('31','SH','上海','shanghai','156',NULL,NULL),('44','GD','广东','guangdong','156',NULL,NULL),('50','ZQ','重庆','chongqing','156',NULL,NULL);
/*!40000 ALTER TABLE `dim_province` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fact_data`
--

DROP TABLE IF EXISTS `fact_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fact_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `project_id` varchar(20) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `station_id` int(11) DEFAULT NULL,
  `carriage_id` varchar(20) DEFAULT NULL,
  `line_id` int(11) DEFAULT NULL,
  `point_system_id` int(11) DEFAULT NULL,
  `point_type_id` int(11) DEFAULT NULL,
  `point_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `reserve_1` int(11) DEFAULT NULL,
  `reserve_2` int(11) DEFAULT NULL,
  `reserve_3` int(11) DEFAULT NULL,
  `reserve_4` int(11) DEFAULT NULL,
  `reserve_5` varchar(128) DEFAULT NULL,
  `reserve_6` varchar(128) DEFAULT NULL,
  `reserve_7` varchar(128) DEFAULT NULL,
  `reserve_8` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fact_data`
--

LOCK TABLES `fact_data` WRITE;
/*!40000 ALTER TABLE `fact_data` DISABLE KEYS */;
INSERT INTO `fact_data` VALUES (1,1,'200237001','2020-01-09 12:47:51','0000-00-00 00:00:00',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `fact_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `line`
--

DROP TABLE IF EXISTS `line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `line` (
  `city_id` int(11) NOT NULL,
  `line_id` int(11) NOT NULL,
  `line_name` varchar(128) DEFAULT NULL,
  `start_station` varchar(128) DEFAULT NULL,
  `stop_station` varchar(128) DEFAULT NULL,
  `crossroads` varchar(128) DEFAULT NULL,
  `start_service_time` datetime DEFAULT NULL,
  `stop_service_time` datetime DEFAULT NULL,
  `mileage` int(11) DEFAULT NULL,
  `total_station` int(11) DEFAULT NULL,
  `vehicle_format` varchar(128) DEFAULT NULL,
  `track_standard` varchar(128) DEFAULT NULL,
  `reserve_1` int(11) DEFAULT NULL,
  `reserve_2` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`city_id`,`line_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `line`
--

LOCK TABLES `line` WRITE;
/*!40000 ALTER TABLE `line` DISABLE KEYS */;
/*!40000 ALTER TABLE `line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` varchar(128) DEFAULT NULL,
  `project_name` varchar(128) DEFAULT NULL,
  `part_A` varchar(128) DEFAULT NULL,
  `part_B` varchar(128) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `reserve_1` int(11) DEFAULT NULL,
  `reserve_2` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1,'200237001','上海一号线','上海申通地铁公司','SRCC','2020-01-09 12:44:37',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `station`
--

DROP TABLE IF EXISTS `station`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `station` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `station_name` varchar(20) DEFAULT NULL,
  `service_time` datetime DEFAULT NULL,
  `belong_to_line` varchar(20) DEFAULT NULL,
  `overground` varchar(20) DEFAULT NULL,
  `if_exchange` tinyint(1) DEFAULT NULL,
  `reserve_1` int(11) DEFAULT NULL,
  `reserve_2` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `station`
--

LOCK TABLES `station` WRITE;
/*!40000 ALTER TABLE `station` DISABLE KEYS */;
INSERT INTO `station` VALUES (1,'上海南站','2020-01-09 12:47:44','1,3','地下',1,NULL,NULL);
/*!40000 ALTER TABLE `station` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_admin`
--

DROP TABLE IF EXISTS `tb_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(100) NOT NULL,
  `_password` varchar(100) NOT NULL,
  `img` varchar(100) NOT NULL,
  `account` varchar(32) NOT NULL,
  `sex` smallint(6) NOT NULL,
  `state` smallint(6) NOT NULL,
  `_last_time` int(11) NOT NULL,
  `_create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_admin`
--

LOCK TABLES `tb_admin` WRITE;
/*!40000 ALTER TABLE `tb_admin` DISABLE KEYS */;
INSERT INTO `tb_admin` VALUES (1,'zhou','pbkdf2:sha256:50000$i5f5GkRm$af61682b687c3ce27462314a53c142300df1dbba9c072a075ff1272cae156be2','','adminn',1,0,1579097200,1578748279),(3,'test','pbkdf2:sha256:50000$II0medr6$fefc252fb793b17a7d6f7dfb9b2361867bf7de76f48044e269ec28b30667d6a1','/static/uploads/image/2018/6/3/1528021622.jpg','123456',1,1,1528045563,1528011755);
/*!40000 ALTER TABLE `tb_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_config_field`
--

DROP TABLE IF EXISTS `tb_config_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_config_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `k` varchar(50) NOT NULL,
  `v` text,
  `type` varchar(50) NOT NULL,
  `desc` text,
  `sorts` int(11) NOT NULL,
  `texttype` varchar(100) NOT NULL,
  `textvalue` varchar(100) NOT NULL,
  `_create_time` int(11) NOT NULL,
  `prompt` varchar(250) NOT NULL,
  `state` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_config_field`
--

LOCK TABLES `tb_config_field` WRITE;
/*!40000 ALTER TABLE `tb_config_field` DISABLE KEYS */;
INSERT INTO `tb_config_field` VALUES (1,'11111111','image_size','上传图片大小','1024：1KB，1048576：1MB，5242880：5MB。建议不要超过1MB，避免图片上传失败',1,'text','',1527989310,'up',1),(2,'1111111','file_size','上传文件大小','1024：1KB，1048576：1MB，5242880：5MB。建议不要超过5MB，避免文件上传失败',2,'text','',1527989343,'up',1),(13,'11111111','media_size','上传音频大小','1024：1KB，1048576：1MB，5242880：5MB。建议不要超过5MB，避免音频上传失败',99,'text','',1528032165,'up',1),(14,'11111111','flash_size','上传flash大小','1024：1KB，1048576：1MB，5242880：5MB。建议不要超过5MB，避免上传失败',99,'text','',1528032209,'up',1);
/*!40000 ALTER TABLE `tb_config_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_log`
--

DROP TABLE IF EXISTS `tb_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_type` varchar(32) NOT NULL,
  `ip` varchar(64) NOT NULL,
  `log_detail` varchar(128) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `_create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log`
--

LOCK TABLES `tb_log` WRITE;
/*!40000 ALTER TABLE `tb_log` DISABLE KEYS */;
INSERT INTO `tb_log` VALUES (1,'login','127.0.0.1','登录成功',2,1578748303),(2,'login','127.0.0.1','登录成功',2,1578748329),(3,'login','127.0.0.1','登录成功',2,1578748503),(4,'login','127.0.0.1','登录成功',2,1578749962),(5,'login','127.0.0.1','登录成功',2,1578749994),(6,'login','127.0.0.1','登录成功',1,1578754571),(7,'login','127.0.0.1','登录成功',1,1578755362),(10,'edit','127.0.0.1','修改配置字段成功',1,1527989400),(193,'login','127.0.0.1','登录成功',1,1528044105),(194,'login','127.0.0.1','登录成功',1,1528044475),(195,'login','127.0.0.1','登录成功',3,1528044539),(196,'login','127.0.0.1','登录成功',3,1528044787),(197,'login','127.0.0.1','登录成功',3,1528044836),(198,'login','127.0.0.1','登录成功',3,1528045453),(199,'login','127.0.0.1','登录成功',3,1528045563),(200,'login','127.0.0.1','登录成功',1,1578812484),(201,'delete','127.0.0.1','修改配置字段成功',1,1578813456),(202,'add','127.0.0.1','增加角色成功',1,1578813813),(203,'login','127.0.0.1','登录成功',1,1578833745),(204,'login','127.0.0.1','登录成功',1,1578897964),(205,'login','127.0.0.1','登录成功',1,1578898947),(206,'login','127.0.0.1','登录成功',1,1578899601),(207,'download','127.0.0.1','下载备份失败！',1,1578900197),(208,'login','127.0.0.1','登录成功',1,1579097200);
/*!40000 ALTER TABLE `tb_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_role`
--

DROP TABLE IF EXISTS `tb_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) NOT NULL,
  `role_type` smallint(6) NOT NULL,
  `describe` text,
  `_role_pri` text,
  `_create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_role`
--

LOCK TABLES `tb_role` WRITE;
/*!40000 ALTER TABLE `tb_role` DISABLE KEYS */;
INSERT INTO `tb_role` VALUES (1,'超管',1,'最高管理员','',1527956507),(2,'普管',2,'普通管理员','1,11,2,21,211,212,213,22,221,222,223,3,31,311,312,313,32,33,34,341',1527956524),(3,'普通的超级管理员',2,'','1,2,3',1578813813);
/*!40000 ALTER TABLE `tb_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_role_admin`
--

DROP TABLE IF EXISTS `tb_role_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_role_admin` (
  `role_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`admin_id`),
  KEY `admin_id` (`admin_id`),
  CONSTRAINT `tb_role_admin_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `tb_admin` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tb_role_admin_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `tb_role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_role_admin`
--

LOCK TABLES `tb_role_admin` WRITE;
/*!40000 ALTER TABLE `tb_role_admin` DISABLE KEYS */;
INSERT INTO `tb_role_admin` VALUES (1,1),(2,3);
/*!40000 ALTER TABLE `tb_role_admin` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-28 11:33:25
