

-- --------------------------------准备设置------------------------------------------------------



show variables like ‘log_bin_trust_function_creators’
set global log_bin_trust_function_creators=1;

-- --------------------------------随机生成函数------------------------------------------------------


# 随机字符串
DELIMITER $$
CREATE FUNCTION random_string(n INT) RETURNS VARCHAR(255)
BEGIN
 DECLARE chars_str VARCHAR(100) DEFAULT 'abcdefghijklmnopqrstuvwxyzABCDEFJHIJKLMNOPQRSTUVWXYZ';
 DECLARE return_str VARCHAR(255) DEFAULT '';
 DECLARE i INT DEFAULT 0;
 WHILE i < n DO
 SET return_str =CONCAT(return_str,SUBSTRING(chars_str,FLOOR(1+RAND()*52),1));
 SET i = i + 1;
 END WHILE;
 RETURN return_str;
END $$


# 随机数字
DELIMITER $$
CREATE FUNCTION random_num( ) 
RETURNS INT(5)  
BEGIN   
 DECLARE i INT DEFAULT 0;  
 SET i = FLOOR(100+RAND()*10);  
RETURN i;  
 END $$

-- --------------------------------插入 存储过程------------------------------------------------------

DELIMITER $$
CREATE PROCEDURE insert_fact_data(IN START INT(10),IN max_num INT(10))  
BEGIN  
DECLARE i INT DEFAULT 0;   
 SET autocommit = 0;    
 REPEAT  
 SET i = i + 1;  
 INSERT INTO fact_data 
(id,city_id,project_id,start_time,end_time,station_id,carriage_id,line_id,point_system_id,point_type_id,point_id,employee_id
	,reserve_1,reserve_2,reserve_3,reserve_4,reserve_5,reserve_6,reserve_7,reserve_8)

 VALUES (START+i,random_num(),random_string(10),now(),now(),random_num(),random_string(10),random_num(),random_num(),random_num(),random_num(),random_num()
 	,random_num(),random_num(),random_num(),random_num(),random_string(6),random_string(10),random_string(20),random_string(16));  
 UNTIL i = max_num  
 END REPEAT;  
 COMMIT;  
 END $$



DELIMITER $$
CREATE PROCEDURE insert_carriages(IN START INT(10),IN max_num INT(10))  
BEGIN  
DECLARE i INT DEFAULT 0;   
 SET autocommit = 0;    
 REPEAT  
 SET i = i + 1;  
 INSERT INTO carriages 
(carriage_id,make_company,marshall_way,marshall_info,buy_bacth,power_supply_way,seat_capacity,bogie,reserve_1,reserve_2)

 VALUES (random_string(10),random_string(10),random_string(6),random_string(10),random_string(20),random_string(16),random_string(16),random_string(16),random_num(),random_string(16));  
 UNTIL i = max_num  
 END REPEAT;  
 COMMIT;  
 END $$



--  DELIMITER $$
-- CREATE PROCEDURE insert_station(IN START INT(10),IN max_num INT(10))  
-- BEGIN  
-- DECLARE i INT DEFAULT 0;   
--  SET autocommit = 0;    
--  REPEAT  
--  SET i = i + 1;  
--  INSERT INTO station 
-- (id,station_name,service_time,belong_to_line,overground,if_exchange,reserve_1,reserve_2)

-- VALUES (START+i,random_string(10),now(),random_string(10),random_string(6),random_num(),random_num(),random_string(20))
--  UNTIL i = max_num  
--  END REPEAT;  
--  COMMIT;  
--  END $$




DELIMITER $$
CREATE PROCEDURE insert_dim_city(IN START INT(10),IN max_num INT(10))  
BEGIN  
DECLARE i INT DEFAULT 0;   
 SET autocommit = 0;    
 REPEAT  
 SET i = i + 1;  
 INSERT INTO dim_city 
(city_id,city_code,city_name_cn,city_name_en,province_id,country_id,reserve_1,reserve_2)

 VALUES (START+i,random_string(10),random_string(10),random_string(6),random_string(10),random_string(20),random_num(),random_string(16));  
 UNTIL i = max_num  
 END REPEAT;  
 COMMIT;  
 END $$




DELIMITER $$
CREATE PROCEDURE insert_dim_country(IN START INT(10),IN max_num INT(10))  
BEGIN  
DECLARE i INT DEFAULT 0;   
 SET autocommit = 0;    
 REPEAT  
 SET i = i + 1;  
 INSERT INTO dim_country 
(country_id,country_2code,country_3code,country_name_cn,country_name_cn_short,country_name_en,reserve_1,reserve_2)

 VALUES (random_string(10),random_string(10),random_string(6),random_string(10),random_string(20),random_string(20),random_num(),random_string(16));  
 UNTIL i = max_num  
 END REPEAT;  
 COMMIT;  
 END $$




 DELIMITER $$
CREATE PROCEDURE insert_dim_province(IN START INT(10),IN max_num INT(10))  
BEGIN  
DECLARE i INT DEFAULT 0;   
 SET autocommit = 0;    
 REPEAT  
 SET i = i + 1;  
 INSERT INTO dim_province 
(province_id,province_code,province_name_cn,province_name_en,country_id,reserve_1,reserve_2)
 VALUES (random_string(10),random_string(10),random_string(6),random_string(10),random_string(20),random_num(),random_string(16));  
 UNTIL i = max_num  
 END REPEAT;  
 COMMIT;  
 END $$


 
-- --------------------------------调用 存储过程------------------------------------------------------


#执行存储过程，往sys_user表添加50万条数据

CALL  insert_fact_data(10,50000); 

CALL  insert_carriages(10,5000); 

CALL  insert_dim_city(10,1000); 

CALL  insert_dim_country(10,100); 

CALL  insert_dim_province(10,500); 

-- CALL  insert_station(10,50000); 



