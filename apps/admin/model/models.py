# -*- coding: utf-8 -*-
# @Time    : 2020/1/13 15:34
# @Author  : Daniel Zhou
# @FileName: models.py
# @Software: PyCharm
# @Blog    ：https://blog.csdn.net/qq_41059320


import influxalchemy
from datetime import datetime
from exts import db
import time

# =========================
# ===== mysql =============
# =========================

# 国家
class Dim_Country(db.Model):
    __tablename__ = 'dim_country'
    # __table_args__ = {'extend_existing': True}
    country_id = db.Column(db.String(20), primary_key=True, unique=True)
    country_2code = db.Column(db.String(20), nullable=False)
    country_3code = db.Column(db.String(20), nullable=False)
    country_name_cn = db.Column(db.String(128), nullable=False)
    country_name_cn_short = db.Column(db.String(128), nullable=False)
    country_name_en = db.Column(db.String(128), nullable=False)
    reserve_1 = db.Column(db.Integer)
    reserve_2 = db.Column(db.String(128))


# country_id=784, country_2code="AE", country_3code="ARE", country_name_cn="阿拉伯联合酋长国",
# country_name_cn_short="阿联酋", country_name_en="The United Arab Emirates"


# 省份
class Dim_Province(db.Model):
    __tablename__ = 'dim_province'
    # __table_args__ = {'extend_existing': True}
    province_id = db.Column(db.String(20), primary_key=True, unique=True)
    province_code = db.Column(db.String(20), nullable=False)
    province_name_cn = db.Column(db.String(128), nullable=False)
    province_name_en = db.Column(db.String(128), nullable=False)
    country_id = db.Column(db.String(20), nullable=False)
    reserve_1 = db.Column(db.Integer)
    reserve_2 = db.Column(db.String(128))


# 城市
class Dim_City(db.Model):
    __tablename__ = 'dim_city'
    # __table_args__ = {'extend_existing': True}
    city_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    city_code = db.Column(db.String(20), nullable=False)
    city_name_cn = db.Column(db.String(128), nullable=False)
    city_name_en = db.Column(db.String(128), nullable=False)
    province_id = db.Column(db.String(20), nullable=False)
    country_id = db.Column(db.String(20), nullable=False)
    reserve_1 = db.Column(db.Integer)
    reserve_2 = db.Column(db.String(128))

    # def __repr__(self):
    #     return 'Note body: {}'.format(self.body)

# 车体
class Carriages(db.Model):
    __tablename__ = 'carriages'
    # __table_args__ = {'extend_existing': True}
    carriage_id = db.Column(db.String(28), primary_key=True)
    make_company = db.Column(db.String(128))
    marshall_way = db.Column(db.String(128))
    marshall_info = db.Column(db.String(128))
    buy_bacth = db.Column(db.String(128))
    power_supply_way = db.Column(db.String(128))
    seat_capacity = db.Column(db.String(128))
    bogie = db.Column(db.String(128))
    reserve_1 = db.Column(db.Integer)
    reserve_2 = db.Column(db.String(128))


# 线路
class Line(db.Model):
    __tablename__ = 'line'
    # __table_args__ = {'extend_existing': True}
    city_id = db.Column(db.Integer, primary_key=True)
    line_id = db.Column(db.Integer, primary_key=True)
    line_name = db.Column(db.String(128))
    start_station = db.Column(db.String(128))
    stop_station = db.Column(db.String(128))
    crossroads = db.Column(db.String(128))
    start_service_time = db.Column(db.DateTime, default=datetime.now())
    stop_service_time = db.Column(db.DateTime, default=datetime.now)
    mileage = db.Column(db.Integer)
    total_station = db.Column(db.Integer)
    vehicle_format = db.Column(db.String(128))
    track_standard = db.Column(db.String(128))
    reserve_1 = db.Column(db.Integer)
    reserve_2 = db.Column(db.String(128))


# 项目
class Project(db.Model):
    __tablename__ = 'project'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    project_id = db.Column(db.String(128))
    project_name = db.Column(db.String(128))
    part_A = db.Column(db.String(128))
    part_B = db.Column(db.String(128))
    start_time = db.Column(db.DateTime, default=datetime.now())
    end_time = db.Column(db.DateTime, default=datetime.now)
    update_time = db.Column(db.DateTime, default=datetime.now)
    reserve_1 = db.Column(db.Integer)
    reserve_2 = db.Column(db.String(128))

# 车站
class Station(db.Model):
    __tablename__ = 'station'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    station_name = db.Column(db.String(20))
    service_time = db.Column(db.DateTime, default=datetime.now())
    belong_to_line = db.Column(db.String(20))
    overground = db.Column(db.String(20))
    if_exchange = db.Column(db.Boolean)
    reserve_1 = db.Column(db.Integer)
    reserve_2 = db.Column(db.String(128))


# 数据
class Fact_Data(db.Model):
    __tablename__ = 'fact_data'
    # __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    city_id = db.Column(db.Integer, nullable=False)
    project_id = db.Column(db.String(20), nullable=False)
    start_time = db.Column(db.DateTime, default=datetime.now())
    end_time = db.Column(db.DateTime, default=datetime.now)
    station_id = db.Column(db.Integer)
    carriage_id = db.Column(db.String(20))
    line_id = db.Column(db.Integer)
    point_system_id = db.Column(db.Integer)
    point_type_id = db.Column(db.Integer)
    point_id = db.Column(db.Integer)
    employee_id = db.Column(db.Integer)
    reserve_1 = db.Column(db.Integer)
    reserve_2 = db.Column(db.Integer)
    reserve_3 = db.Column(db.Integer)
    reserve_4 = db.Column(db.Integer)
    reserve_5 = db.Column(db.String(128))
    reserve_6 = db.Column(db.String(128))
    reserve_7 = db.Column(db.String(128))
    reserve_8 = db.Column(db.String(128))

# =========================
# ===== influxdb =============
# =========================


class Conjunction_Demo(influxalchemy.Measurement):
    __measurement__ = 'conjunction_demo'

