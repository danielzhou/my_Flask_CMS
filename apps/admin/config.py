# 配置文件 
'''
    设置菜单
    :param id:
        菜单id 和角色关联.
    :param pri_name:
        权限名称
    :param url_prefix:
        蓝图 或者 也可以成为模块名称
    :param actin_name:
        函数名称
'''
def set_menu(id,icon,pri_name,url_prefix,action_name,child_menu=[],is_treeview=True):
    menu = {'id':id,"icon":icon,'pri_name':pri_name,'url_prefix':url_prefix,'action_name':action_name,'is_treeview':is_treeview}
    if child_menu:
        menu['child']=child_menu
    return menu

menu = (
    set_menu(1,'fa fa-fw fa-paste (alias)','首页管理',None,None,child_menu=(
        set_menu(11,'fa fa-fw fa-copy (alias)','Dashboard','adminindex','dashboard',is_treeview=False),
    )),

    set_menu(2,'fa fa-fw fa-user-plus','查询分析',None,None,child_menu=(
        set_menu(21,'fa fa-fw fa-user','条件查询','adminindex','index',child_menu=(
            set_menu(211,'','增加管理员','adminadmin','add'),
            set_menu(212,'','修改管理员','adminadmin','edit'),
            set_menu(213,'','删除管理员','adminadmin','delete'),
        ),is_treeview=False),
        # set_menu(22, 'fa fa-fw fa-user-secret','结果可视化', 'adminrole', 'index', child_menu=(
            # set_menu(221,'', '增加角色', 'adminrole', 'add'),
            # set_menu(222,'', '修改角色', 'adminrole', 'edit'),
            # set_menu(223,'', '删除角色', 'adminrole', 'delete'),
        # ),is_treeview=False),
        set_menu(23, 'fa fa-fw fa-user-secret','历史数据', 'adminrole', 'index', child_menu=(
            set_menu(221,'', '增加角色', 'adminrole', 'add'),
            set_menu(222,'', '修改角色', 'adminrole', 'edit'),
            set_menu(223,'', '删除角色', 'adminrole', 'delete'),
        ),is_treeview=False),

    )),
    set_menu(3,'fa fa-fw fa-calculator','计算处理',None,None,child_menu=(
        set_menu(31,'fa fa-fw fa-legal (alias)','业务流程', 'adminconfig_field', 'index', child_menu=(
            set_menu(311,'', '增加配置字段', 'adminconfig_field', 'add'),
            set_menu(312,'','修改配置字段', 'adminconfig_field', 'edit'),
            set_menu(313,'','删除配置字段', 'adminconfig_field', 'delete'),
        ),is_treeview=False),
        set_menu(32,'fa fa-fw fa-sticky-note-o','预处理和雨流', 'adminconfig_field', 'web',is_treeview=False),
        set_menu(33,'fa fa-fw fa-folder-o' ,'疲劳寿命计算', 'adminconfig_field', 'up', child_menu=(
            set_menu(331,'','删除日志', 'adminconfig_field', 'delete'),
        ),is_treeview=False),
        # set_menu(34,'fa fa-fw fa-folder-o' ,'疲劳寿命计算', 'adminconfig_field', 'down', child_menu=(
        #     set_menu(341,'','删除日志', 'adminconfig_field', 'delete'),
        # ),is_treeview=False),
    )),
    # set_menu(4,'fa fa-fw fa-wrench','项目管理',None,None,child_menu=(
    #     set_menu(41,'fa fa-fw fa-legal (alias)','项目管理', 'adminconfig_field', 'index', child_menu=(
    #         set_menu(411,'', '增加配置字段', 'adminconfig_field', 'add'),
    #         set_menu(412,'','修改配置字段', 'adminconfig_field', 'edit'),
    #         set_menu(413,'','删除配置字段', 'adminconfig_field', 'delete'),
    #     ),is_treeview=False),
    # )),
    set_menu(5,'fa fa-fw fa-database','数据管理',None,None,child_menu=(
        set_menu(51,'fa fa-fw fa-legal (alias)','数据录入', 'adminconfig_field', 'index', child_menu=(
            set_menu(511,'', '增加配置字段', 'adminconfig_field', 'add'),
            set_menu(512,'','修改配置字段', 'adminconfig_field', 'edit'),
            set_menu(513,'','删除配置字段', 'adminconfig_field', 'delete'),
        ),is_treeview=False),
        set_menu(52,'fa fa-fw fa-legal (alias)','数据导出', 'adminconfig_field', 'index', child_menu=(
            set_menu(521,'', '增加配置字段', 'adminconfig_field', 'add'),
            set_menu(522,'','修改配置字段', 'adminconfig_field', 'edit'),
            set_menu(523,'','删除配置字段', 'adminconfig_field', 'delete'),
        ),is_treeview=False),
        set_menu(53,'fa fa-fw fa-legal (alias)','备份计划', 'adminconfig_field', 'index', child_menu=(
            set_menu(531,'', '增加配置字段', 'adminconfig_field', 'add'),
            set_menu(532,'','修改配置字段', 'adminconfig_field', 'edit'),
            set_menu(533,'','删除配置字段', 'adminconfig_field', 'delete'),
        ),is_treeview=False),
        set_menu(54,'fa fa-fw fa-legal (alias)','数据恢复', 'adminconfig_field', 'index', child_menu=(
            set_menu(541,'', '增加配置字段', 'adminconfig_field', 'add'),
            set_menu(542,'','修改配置字段', 'adminconfig_field', 'edit'),
            set_menu(543,'','删除配置字段', 'adminconfig_field', 'delete'),
        ),is_treeview=False),
    )),
    set_menu(6,'fa fa-fw fa-cog','系统管理',None,None,child_menu=(
        set_menu(61,'fa fa-fw fa-legal (alias)','用户管理', 'adminadmin', 'index', child_menu=(
            set_menu(611,'', '增加配置字段', 'adminadmin', 'add'),
            set_menu(612,'','修改配置字段', 'adminadmin', 'edit'),
            set_menu(613,'','删除配置字段', 'adminadmin', 'delete'),
        ),is_treeview=False),
        set_menu(62,'fa fa-fw fa-legal (alias)','角色设置', 'adminrole', 'index', child_menu=(
            set_menu(621,'', '增加配置字段', 'adminrole', 'add'),
            set_menu(622,'','修改配置字段', 'adminrole', 'edit'),
            set_menu(623,'','删除配置字段', 'adminrole', 'delete'),
        ),is_treeview=False),
        set_menu(63,'fa fa-fw fa-legal (alias)','日志记录', 'adminlog', 'index', child_menu=(
            set_menu(631,'','删除日志', 'adminlog', 'delete'),
            # set_menu(631,'', '增加配置字段', 'adminconfig_field', 'add'),
            # set_menu(632,'','修改配置字段', 'adminconfig_field', 'edit'),
            # set_menu(633,'','删除配置字段', 'adminconfig_field', 'delete'),
        ),is_treeview=False),
    )),

    #
    # set_menu(2,'fa fa-fw fa-user-plus','权限管理',None,None,child_menu=(
    #     set_menu(21,'fa fa-fw fa-user','管理员列表','adminadmin','index',child_menu=(
    #         set_menu(211,'','增加管理员','adminadmin','add'),
    #         set_menu(212,'','修改管理员','adminadmin','edit'),
    #         set_menu(213,'','删除管理员','adminadmin','delete'),
    #     ),is_treeview=False),
    #     set_menu(22, 'fa fa-fw fa-user-secret','角色列表', 'adminrole', 'index', child_menu=(
    #         set_menu(221,'', '增加角色', 'adminrole', 'add'),
    #         set_menu(222,'', '修改角色', 'adminrole', 'edit'),
    #         set_menu(223,'', '删除角色', 'adminrole', 'delete'),
    #     ),is_treeview=False),
    #
    # )),
    # set_menu(3,'fa fa-fw fa-wrench','系统管理',None,None,child_menu=(
    #     set_menu(31,'fa fa-fw fa-legal (alias)','用户管理', 'adminconfig_field', 'index', child_menu=(
    #         set_menu(311,'', '增加配置字段', 'adminconfig_field', 'add'),
    #         set_menu(312,'','修改配置字段', 'adminconfig_field', 'edit'),
    #         set_menu(313,'','删除配置字段', 'adminconfig_field', 'delete'),
    #     ),is_treeview=False),
    #     set_menu(32,'fa fa-fw fa-sticky-note-o','角色管理', 'adminconfig_field', 'web',is_treeview=False),
    #     # set_menu(33,'fa fa-fw fa-cloud-upload','上传设置', 'adminconfig_field', 'up', is_treeview=False),
    #     set_menu(33,'fa fa-fw fa-folder-o' ,'日志列表', 'adminlog', 'index', child_menu=(
    #         set_menu(341,'','删除日志', 'adminlog', 'delete'),
    #     ),is_treeview=False),
    # )),
)

role_type = {1:'超级管理员',2:'普通管理员',3:'普通员工'}

ADMIN_SESSION_ID = 'a6273a8b622104d4d63d0'
# ADMIN_SESSION_ID = 'x'

# 分页大小
PAGE_SIZE = 15

