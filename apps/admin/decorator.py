# _*_ conding:utf-8 _*_
'''用于限制用户访问'''
#
# from functools import wraps
# from flask import session, redirect, url_for
# from .config import ADMIN_SESSION_ID
# from .model.admin import Admin
#
# # 登录限制
# def login_required(func):
#     @wraps(func)
#     def wrapper(*args, **kwargs):
#         admin_id = session.get(ADMIN_SESSION_ID)
#         admin = Admin.query.get(admin_id)
#         if admin:
#             return func(*args, **kwargs)
#         else:
#             return redirect(url_for('adminlogin.login'))
#
#     return wrapper