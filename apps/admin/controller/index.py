from flask import Blueprint,render_template,session
from flask_login import login_required
import influxdb
import influxalchemy
import pandas as pd
from ..model.models import Dim_Country, Dim_Province, Dim_City, Conjunction_Demo
bp = Blueprint('adminindex',__name__,url_prefix='/admin/index')

# influxdb 连接
influx_client = influxdb.DataFrameClient(database="test")
flux = influxalchemy.InfluxAlchemy(influx_client)

@bp.route('/dashboard/')
# @login_required
def dashboard():
    # city =  "上海" #Request.params.get("city")  # 上海
    # # project = Request.params.get("project")
    # # line = Request.params.get("line")
    # # carriage = Request.params.get("carriage")
    # # station = Request.params.get("station")
    #
    # query = flux.query(Conjunction_Demo).filter(Conjunction_Demo.cities == city).limit(5)
    # result_list = query.execute()  # <collections.defaultdict>
    # df = pd.DataFrame()
    # for k, v in result_list.items():
    #     df = v
    # labels = df.columns.tolist()
    # print(labels)
    # content = []
    # for row in df.iterrows():
    #     index, data = row
    #     content.append(data.tolist())
    # print(content)
    # return render_template("admin/index/index.html", columns=labels, data=content)
    return render_template("admin/index/dashboard.html")


@bp.route('/index/')
# @login_required
def index():
    city =  "上海" #Request.params.get("city")  # 上海
    # project = Request.params.get("project")
    # line = Request.params.get("line")
    # carriage = Request.params.get("carriage")
    # station = Request.params.get("station")

    query = flux.query(Conjunction_Demo).filter(Conjunction_Demo.cities == city).limit(5)
    result_list = query.execute()  # <collections.defaultdict>
    df = pd.DataFrame()
    for k, v in result_list.items():
        df = v
    labels = df.columns.tolist()[:-1]
    print(labels)
    content = []
    for row in df.iterrows():
        index, data = row
        # print(type(data))
        # print(data)
        # data = data[:-1]
        content.append(data.tolist()[:-1])
    print(content[0])
    return render_template("admin/index/index.html", columns=labels, data=content)
    # return render_template("admin/index/dashboard.html", columns=labels, data=content)




@bp.route('/history/')
# @login_required
def history():
    city =  "上海" #Request.params.get("city")  # 上海
    # project = Request.params.get("project")
    # line = Request.params.get("line")
    # carriage = Request.params.get("carriage")
    # station = Request.params.get("station")

    query = flux.query(Conjunction_Demo).filter(Conjunction_Demo.cities == city).limit(5)
    result_list = query.execute()  # <collections.defaultdict>
    df = pd.DataFrame()
    for k, v in result_list.items():
        df = v
    labels = df.columns.tolist()[:-1]
    print(labels)
    content = []
    for row in df.iterrows():
        index, data = row
        # print(type(data))
        # print(data)
        # data = data[:-1]
        content.append(data.tolist()[:-1])
    print(content[0])
    return render_template("admin/index/history.html", columns=labels, data=content)
    # return render_template("admin/index/dashboard.html", columns=labels, data=content)
