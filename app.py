import datetime
from flask import Flask, session, redirect, url_for
from apps.admin.controller.database import bp as admindatabasebp
from apps.admin.controller.log import bp as adminlogbp
from apps.admin.controller.config_field import bp as adminconfig_fieldbp
from apps.admin.controller.upload import bp as adminuploadbp
from apps.admin.controller.index import bp as adminindexbp
from apps.common.controller.tool import bp as commontoolbp
import config
import apps.admin.hooks
from exts import db
from flask_session import Session
from apps.home.controller.index import bp as homeindexbp
from apps.admin.controller.login import bp as adminloginbp, LoginView
from apps.admin.controller.role import bp as adminrolebp
from apps.admin.controller.admin import bp as adminadminbp
# login required test
from flask_login.login_manager import LoginManager
from flask_login import login_user, logout_user, UserMixin
from flask_bootstrap import Bootstrap

class User(UserMixin):
    def is_authenticated(self):
        return True
    def is_active(self):
        return True
    def is_anonymous(self):
        return False
    def get_id(self):
        return "1"

# # # # # # # # # # # # # # # # # # # # # from think.library.build import Build
# # # # # # # # # # # # # # # # # # # # # Build().run()
app = Flask(__name__)
bootstrap = Bootstrap(app)

app.register_blueprint(admindatabasebp)
app.register_blueprint(adminlogbp)
app.register_blueprint(adminconfig_fieldbp)
app.register_blueprint(adminuploadbp)
app.register_blueprint(adminindexbp)
app.register_blueprint(commontoolbp)
app.config.from_object(config)
app.register_blueprint(homeindexbp)
app.register_blueprint(adminloginbp)
app.register_blueprint(adminrolebp)
app.register_blueprint(adminadminbp)
# login required test
app.secret_key = 's3cr3t'
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'login'
login_manager.init_app(app)
Session(app)
# session.permanent = True
# app.permanent_session_lifetime = datetime.timedelta(seconds=5)  # 5秒真男人
db.init_app(app)

@login_manager.user_loader
def load_user(user_id):
    # print(user_id)
    user = User()
    return user

@app.route("/login/")
def login():
    user = User()
    login_user(user)
    return redirect('/admin/login/login/')

@app.template_global('table_sort')
def table_sort(param):
    '''
        所有蓝图公用的点击排序
    :param param:排序的字段
    :return:
    '''
    param = str(param)
    from flask import request
    url_path = request.path
    faStr = 'fa-sort'
    get = request.args.to_dict()
    if '_pjax' in get:
        get.pop('_pjax')
    if '_sort' in get:
        sortArr = get.get('_sort').split(',')
        if sortArr[0] == param:
            if sortArr[1] == 'asc':
                faStr = 'fa-sort-asc'
                sort = 'desc'
            elif sortArr[1] == 'desc':
                faStr = 'fa-sort-desc'
                sort = 'asc'
            get['_sort'] = param+','+sort
        else:
            get['_sort'] = param+',asc'
    else:
        get['_sort'] = param+ ',asc'
    paramStr = [];
    for v in get:
        paramStr.append(v+'='+get[v])
    paramStrs = "&".join(paramStr)
    url_path = url_path + '?' + paramStrs
    return '<a class="fa '+faStr+'" href="'+url_path+'"></a>'

@app.template_global('search_url')
def search_url(param):
    '''
    搜索
    :param param:
    :return:
    '''
    param = str(param)
    from flask import request
    url_path = request.path
    get = request.args.to_dict()
    if '_pjax' in get:
        get.pop('_pjax')
    if param in get:
        get.pop(param)
    if 'page' in get:
        get.pop('page')
    if get:
        paramStr = []
        for v in get:
            paramStr.append(v + '=' + get[v])
        paramStrs = "&".join(paramStr)
        url_path = url_path + '?' + paramStrs
    return url_path
if __name__ == '__main__':
    app.run(debug=True)