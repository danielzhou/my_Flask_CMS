# my_Flask_CMS

### 步骤

1. 环境准备

    config.py中的数据库配置
    启动redis服务
    
2. 数据库迁移
    
    python maanger.py mg stamp head
    python maanger.py mg migrate
    python maanger.py mg upgrade
    
3. 测试数据

    - 添加第一个user
    python manager.py  build
    python manage.py add_root -a zhou -c adminn -p adminn
    - 添加其他数据
    运行test.sql中的insert

4. 启动

     python manage.py runserver