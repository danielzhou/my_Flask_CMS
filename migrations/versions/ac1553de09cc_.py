"""empty message

Revision ID: ac1553de09cc
Revises: 33ccaa997ac7
Create Date: 2020-01-13 21:44:56.090233

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ac1553de09cc'
down_revision = '33ccaa997ac7'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('dim_city_ibfk_2', 'dim_city', type_='foreignkey')
    op.drop_constraint('dim_province_ibfk_1', 'dim_province', type_='foreignkey')
    op.add_column('station', sa.Column('station_name', sa.String(length=20), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('station', 'station_name')
    op.create_foreign_key('dim_province_ibfk_1', 'dim_province', 'dim_country', ['country_id'], ['country_id'])
    op.create_foreign_key('dim_city_ibfk_2', 'dim_city', 'dim_province', ['province_id'], ['province_id'])
    # ### end Alembic commands ###
